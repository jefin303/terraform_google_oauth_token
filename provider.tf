terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.27.0"
    }
  }
}

provider "google" {

      #Using access token to call GCP APIs   
   #access_token=$GOOGLE_OAUTH_ACCESS_TOKEN
     # GCP project ID
   project     = "mu-new-project-1234"
      # Any region of your choice
   region      = "us-central1"
      # Any zone of your choice      
   zone        = "us-central1-a"
}
